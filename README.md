# GitLab Wizard Browser Extension

The **GitLab Wizard** browser extension improves your GitLab workflow.

It's a cross-browser extension that works in Google Chrome, Firefox and some more browsers that support the Web Extension API.

## Installation / Download

Just select your browser:

[![Chrome](assets/chrome.png 'Chrome')](https://chrome.google.com/webstore/detail/gitlab-wizard/ljjhoenlpeipbmmfpkhkcoijlbifebpo)
[![Firefox](assets/firefox.png 'Firefox')](https://addons.mozilla.org/de/firefox/addon/gitlab-wizard)

## Features

On the options page you can define presets that you want to be filled into your merge requests so you don't have to fill all these options by hand.

-   fill approval group (approvers + required approvals)
-   fill labels
-   choose MR template
-   assign MR to yourself
-   set option _Squash commits_
-   set option _Delete source branch_

### Planned features

-   feel free to submit any suggestions :)

## Development

Feel free to submit issues or merge requests to this project.
The browser extension is written using the [Web Extension API](https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions), [Vue.js](https://vuejs.org/) and [BootstrapVue](https://bootstrap-vue.org/).

### Steps for developing

1. Clone repository
2. Install dependencies: `$ npm install`
3. Build extension: `$ npm run watch:dev`
4. Load extension in the browser:
    - Chrome:
        1. Visit [chrome://extensions/](chrome://extensions/)
        2. Enable Developer mode
        3. Click `Load unpacked extension` and select the `dist` folder.
    - Firefox:
        1. Visit [about:debugging#/runtime/this-firefox](about:debugging#/runtime/this-firefox)
        2. Click `Load Temporary Add-on` and select the `dist/manifest.json` file.

## Building the extension for production environment

### Requirements

-   Software:
    -   Node >= v14 https://nodejs.org/de/download/ (used for submitted build: v14.13.0)
    -   NPM >= v6 (included in Node Download) (used for submitted build: 6.14.8)

### Build steps

1. Go to the root directory (containing the `package-lock.json`)
2. Install dependencies: `$ npm install`
3. Run `npm run build` for building the `/dist` directory
4. Run `npm run build-zip` for packing the `/dist` directory as ZIP archive
