import { setChecked } from '../../utils/inputUtils.js';

const fillOptions = async ({ deleteSourceBranch, squashCommits, assignToMe }) => {
    const deleteSourceBranchCheckbox = document.querySelector('#merge_request_force_remove_source_branch');
    if (deleteSourceBranch && deleteSourceBranchCheckbox) {
        setChecked(deleteSourceBranchCheckbox);
    }

    const squashCommitsCheckbox = document.querySelector('#merge_request_squash');
    if (squashCommits && squashCommitsCheckbox) {
        setChecked(squashCommitsCheckbox);
    }

    if (assignToMe) {
        const assignToMeLink = document.querySelector('.merge-request-assignee .assign-to-me-link');
        if (getComputedStyle(assignToMeLink).display !== 'none') {
            assignToMeLink.click();
        }
    }
};

export default fillOptions;
