const EMPTY_PRESET = () => ({
    presetName: 'Name',
    approvers: [],
    labels: [],
    squashCommits: false,
    deleteSourceBranch: false,
    assignToMe: false,
    numberApprovalsRequired: null,
    approversAsReviewers: false,
    template: '',
    isExpanded: true,
});

const state = {
    presets: [],
};

const mutations = {
    addPreset(state) {
        state.presets.push({ ...EMPTY_PRESET() });
    },
    removePreset(state, presetIndex) {
        state.presets.splice(presetIndex, 1);
    },
    setPresetName(state, { presetIndex, name }) {
        state.presets[presetIndex].presetName = name;
    },
    addApprover(state, { presetIndex, approver }) {
        state.presets[presetIndex].approvers.push(approver);
    },
    removeApprover(state, { presetIndex, approverIndex }) {
        state.presets[presetIndex].approvers.splice(approverIndex, 1);
    },
    renameApprover(state, { presetIndex, approverIndex, approver }) {
        state.presets[presetIndex].approvers[approverIndex] = approver.replace('@', '');
    },
    setApproversAsReviewers(state, { presetIndex, value }) {
        state.presets[presetIndex].approversAsReviewers = value;
    },
    addLabel(state, { presetIndex, label }) {
        state.presets[presetIndex].labels.push(label);
    },
    removeLabel(state, { presetIndex, labelIndex }) {
        state.presets[presetIndex].labels.splice(labelIndex, 1);
    },
    renameLabel(state, { presetIndex, labelIndex, label }) {
        state.presets[presetIndex].labels[labelIndex] = label;
    },
    setSquashCommits(state, { presetIndex, value }) {
        state.presets[presetIndex].squashCommits = value;
    },
    setDeleteSourceBranch(state, { presetIndex, value }) {
        state.presets[presetIndex].deleteSourceBranch = value;
    },
    setAssignToMe(state, { presetIndex, value }) {
        state.presets[presetIndex].assignToMe = value;
    },
    setNumberApprovalsRequired(state, { presetIndex, value }) {
        state.presets[presetIndex].numberApprovalsRequired = value;
    },
    setTemplate(state, { presetIndex, value }) {
        state.presets[presetIndex].template = value;
    },
    setIsExpanded(state, { presetIndex, value }) {
        state.presets[presetIndex].isExpanded = value;
    },
    addPresets(state, data) {
        state.presets.push(...data);
    },
};

export default {
    namespaced: true,
    state,
    mutations,
};
