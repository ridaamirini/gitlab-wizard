export const getUserNameFromElement = element => {
    if (!element) {
        return null;
    }

    return element.textContent.trim().toLowerCase();
};
