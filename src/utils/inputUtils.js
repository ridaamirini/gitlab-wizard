import sleep from './sleep';

/**
 * Sets the value of an input element and emits the input event.
 * @param {Element} inputElement
 * @param value
 */
export const setValue = (inputElement, value) => {
    inputElement.value = value;
    inputElement.dispatchEvent(new Event('input'));
};

/**
 * Sets a checkbox either to checked or not to checked
 * @param {Element} checkboxElement
 * @param {Boolean} checked
 */
export const setChecked = (checkboxElement, checked = true) => {
    checkboxElement.checked = checked;
    checkboxElement.dispatchEvent(new Event('click'));
    checkboxElement.dispatchEvent(new Event('change'));
};

/**
 * Collects the dropdown values from giving parent and selector
 * @param {Element} parentElement
 * @param {string} valueSelector
 * @param {{sleepTime: number, maxWaitTime: number}} options
 * @returns {Promise<[]>}
 */
export const collectDropdownValues = async (
    parentElement,
    valueSelector,
    options = { sleepTime: 50, maxWaitTime: 5000 }
) => {
    let { maxWaitTime } = options;
    let data = [];

    while (!data.length) {
        data = parentElement.querySelectorAll(valueSelector);

        await sleep(options.sleepTime);

        maxWaitTime -= options.sleepTime;

        if (maxWaitTime <= 0) {
            break;
        }
    }

    return data;
};
